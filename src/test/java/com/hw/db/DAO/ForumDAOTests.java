package com.hw.db.dao;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.DisplayName;

class ForumDAOTests {
    private JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    private ForumDAO forum = new ForumDAO(mockJdbc);
    private UserDAO.UserMapper userMapper = new UserDAO.UserMapper();
    private final String STR_DATE = "01.01.2023";
    private final int INT_THREAD_NUM = 1; 
    private final String STR_SLUG = "slug";         
    private final String STR_SELECT = "SELECT nickname,fullname,email,about FROM forum_users "; 

    @Test
    @DisplayName("ForumDAOTests - Test 1")
    void forumDAOTest_01() {
        ForumDAO.UserList(STR_SLUG, INT_THREAD_NUM, STR_DATE, true);
        verify(mockJdbc).query(Mockito.eq(STR_SELECT + "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("ForumDAOTests - Test 2")
    void forumDAOTest_02() {
        ForumDAO.UserList(STR_SLUG, INT_THREAD_NUM, STR_DATE, false);
        verify(mockJdbc).query(Mockito.eq(STR_SELECT + "WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("ForumDAOTests - Test 3")
    void forumDAOTest_03() {
        ForumDAO.UserList(STR_SLUG, null, null, true);
        verify(mockJdbc).query(Mockito.eq(STR_SELECT + "WHERE forum = (?)::citext ORDER BY nickname desc;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("ForumDAOTests - Test 4")
    void forumDAOTest_04() {
        ForumDAO.UserList(STR_SLUG, null, null, false);
        verify(mockJdbc).query(Mockito.eq(STR_SELECT + "WHERE forum = (?)::citext ORDER BY nickname;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }


    @Test
    @DisplayName("ForumDAOTests - Test 5")
    void forumDAOTest_05() {
        ForumDAO.UserList(STR_SLUG, INT_THREAD_NUM, STR_DATE, true);
        verify(mockJdbc).query(Mockito.eq(STR_SELECT + "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("ForumDAOTests - Test 6")
    void forumDAOTest_06() {
        ForumDAO.UserList(STR_SLUG, INT_THREAD_NUM, null, false);
        verify(mockJdbc).query(Mockito.eq(STR_SELECT + "WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("ForumDAOTests - Test 7")
    void forumDAOTest_07() {
        ForumDAO.UserList(STR_SLUG, null, STR_DATE, false);
        verify(mockJdbc).query(Mockito.eq(STR_SELECT + "WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("ForumDAOTests - Test 8")
    void forumDAOTest_08() {
        ForumDAO.UserList(STR_SLUG, null, STR_DATE, true);
        verify(mockJdbc).query(Mockito.eq(STR_SELECT + "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
    
    @Test
    @DisplayName("ForumDAOTests - Test 9")
    void forumDAOTest_09() {
        ForumDAO.UserList(STR_SLUG, INT_THREAD_NUM, null, true);
        verify(mockJdbc).query(Mockito.eq(STR_SELECT + "WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}