package com.hw.db.dao;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.DisplayName;

class ThreadDAOTests {
    private JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    private ThreadDAO thread = new ThreadDAO(mockJdbc);
    private final int TREE_VALUES[] = {2, 4, 6};

    @Test
    @DisplayName("ThreadDAOTests - Test 1")  
    void threadDAOTest_01() {
        ThreadDAO.treeSort(TREE_VALUES[0], TREE_VALUES[1], TREE_VALUES[2], true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(TREE_VALUES[0]), Mockito.eq(TREE_VALUES[2]), Mockito.eq(TREE_VALUES[1]));
    }

    @Test
    @DisplayName("ThreadDAOTests - Test 2")
    void threadDAOTest_02() {
        ThreadDAO.treeSort(TREE_VALUES[0], TREE_VALUES[1], TREE_VALUES[2], null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(TREE_VALUES[0]), Mockito.eq(TREE_VALUES[2]), Mockito.eq(TREE_VALUES[1]));
    }
}
