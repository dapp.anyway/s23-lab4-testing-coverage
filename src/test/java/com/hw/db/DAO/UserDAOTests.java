package com.hw.db.dao;

import static org.mockito.Mockito.*;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.DisplayName;

class UserDAOTests {
    private final String STR_NICKNAME = "nickname";
    private final String STR_MAIL = "mail";
    private final String STR_FULLNAME = "fullname"; 
    private final String STR_ABOUT = "about";
    private final String STR_UPDATE = "UPDATE \"users\" SET"; 

    @Test
    @DisplayName("UserDAOTests - Test 1")
    void userDAOTest_01() {
        User user = new User(STR_NICKNAME, STR_MAIL, null, STR_ABOUT);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc); UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq(STR_UPDATE + "  email=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.eq(STR_MAIL), Mockito.eq(STR_ABOUT), Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("UserDAOTests - Test 2")
    void userDAOTest_02() {
        User user = new User(STR_NICKNAME, STR_MAIL, STR_FULLNAME, STR_ABOUT);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc); UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq(STR_UPDATE + "  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.eq(STR_MAIL), Mockito.eq(STR_FULLNAME), Mockito.eq(STR_ABOUT), Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("UserDAOTests - Test 3")
    void userDAOTest_03() {
        User user = new User(STR_NICKNAME, STR_MAIL, STR_FULLNAME, null); 
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc); UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq(STR_UPDATE + "  email=? , fullname=?  WHERE nickname=?::CITEXT;"), Mockito.eq(STR_MAIL), Mockito.eq(STR_FULLNAME), Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("UserDAOTests - Test 4")
    void userDAOTest_04() {
        User user = new User(STR_NICKNAME, STR_MAIL, null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc); UserDAO.Change(user);
        verify(mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class));
    }

    @Test
    @DisplayName("UserDAOTests - Test 5")
    void userDAOTest_05() {
        User user = new User(STR_NICKNAME, null, STR_FULLNAME, STR_ABOUT);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc); UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq(STR_UPDATE + "  fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.eq(STR_FULLNAME), Mockito.eq(STR_ABOUT), Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("UserDAOTests - Test 6")
    void userDAOTest_06() {
        User user = new User(STR_NICKNAME, STR_MAIL, null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc); UserDAO.Change(user);
        verify(mockJdbc).update( Mockito.eq(STR_UPDATE + "  email=?  WHERE nickname=?::CITEXT;"), Mockito.eq(STR_MAIL), Mockito.eq(user.getNickname()));
    }

}
