package com.hw.db.dao;

import static org.mockito.Mockito.*;
import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.Test;
import java.sql.Timestamp;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.DisplayName;

class PostDAOTests {
    private final String STR_MSG = "message";
    private final String STR_PREV_MSG = "previous_message";
    private final String STR_AUTHOR = "author";
    private final String STR_PREV_AUTHOR = "previous_author";  

    @Test
    @DisplayName("PostDAOTests - Test 1")
    void postDAOTest_01() {
        Post post = new Post();
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Mockito.when(mockJdbc.queryForObject(Mockito.any(String.class), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt())).thenReturn(post);
        new PostDAO(mockJdbc); PostDAO.setPost(0, post);
        verify(mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class));
    }

    @Test
    @DisplayName("PostDAOTests - Test 2")
    void postDAOTest_02() {
        Post post = new Post(null, new Timestamp(0), null, null, null, null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post prev = new Post(); prev.setCreated(new Timestamp(1));
        Mockito.when(mockJdbc.queryForObject(Mockito.any(String.class), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt())).thenReturn(prev); new PostDAO(mockJdbc); PostDAO.setPost(0, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.eq(new Timestamp(0)), Mockito.eq(0));
    }

    @Test
    @DisplayName("PostDAOTests - Test 3")
    void postDAOTest_03() {
        Post post = new Post(null, new Timestamp(0), null, STR_MSG, null, null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post prev = new Post();
        prev.setMessage(STR_PREV_MSG);
        prev.setCreated(new Timestamp(1));
        Mockito.when(mockJdbc.queryForObject(Mockito.any(String.class), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt())).thenReturn(prev);
        new PostDAO(mockJdbc); PostDAO.setPost(0, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.eq(STR_MSG), Mockito.eq(new Timestamp(0)), Mockito.eq(0));
    }

    @Test
    @DisplayName("PostDAOTests - Test 4")
    void postDAOTest_04() {
        Post post = new Post(STR_AUTHOR, new Timestamp(0), null, STR_MSG, null, null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post prev = new Post();
        prev.setAuthor(STR_PREV_AUTHOR);
        prev.setMessage(STR_PREV_MSG);
        prev.setCreated(new Timestamp(1));
        Mockito.when(mockJdbc.queryForObject(Mockito.any(String.class), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt())).thenReturn(prev);
        new PostDAO(mockJdbc); PostDAO.setPost(0, post);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.eq(STR_AUTHOR), Mockito.eq(STR_MSG), Mockito.eq(new Timestamp(0)), Mockito.eq(0));
    }

}
